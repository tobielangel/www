---
title: "OSPO OnRamp past meetings"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The idea of the OSPO OnRamp meeting series is to provide a low-threshold entry point for organisations who want to exchange and learn about the basics on how to set up an OpenSource Program Office and get started into open source.

Please check the [main OnRamp page](/onramp/) for more information about the next meetings.

The meeting are scheduled for every second Friday of the month from 10:30-12:00.

## Past meetings

(*) These parts have not been recorded.

**Friday, November 17th, 10:30-12:00 CEST** \
Agenda
- Florent Zara (Eclipse) - Update on the OSPO Alliance (10mn)
- Boris Baldassari — Chairperson, [OSPO Alliance](https://ospo-alliance.org/). What’s new in version 1.2 of the Good Governance Initiative?
    - 📊 [View slides](/resources/onramp_20231117/20231117_OSPO_OnRamp_GGI-1.2.pdf)
    - 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1700211741467)
- Open discussion (*) (25 min).

****

**Friday, October 20th, 10:30-12:00 CEST** \
Agenda
- Nicolas Toussaint (Orange Business Services) - Update on the OSPO Alliance (10mn)
- Anne-Marie Scott — Board Chair, [Apereo Foundation](https://www.apereo.org/). OSPO in the Academic World. \
  [View slides](/resources/onramp_20231020/20231020_OSPO_OnRamp_OSPO_Academic_World.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1697790501307)
- Open discussion (*) (30 min).

****

**Friday, June 16th, 10:30-12:00 CEST** \
Agenda
- Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10mn)
- Jan Ainali — Codebase Steward (Foundation for Public Code). Introduction to the Governance game. \
  [View slides](/resources/onramp_20230616/20230616_OSPO_OnRamp_The_Governance_Game.pdf) - [Watch video](https://bbb.opencloud.lu/playback/video/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1686902873481)
- Open discussion (*) (30 min).

****

**Friday, May 26st, 10:30-12:00 CEST** \
Agenda
- Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10 min)
- Wolfgang Gehring — FOSS ambassador and OSPO Leader (Mercedes-Benz Tech Innovation). A Vision of FOSS @Mercedes-Benz. \
  [View slides](/resources/onramp_20230526/20230526_OSPO_OnRamp_Mercedes-Benz_OSPO.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1685090362627)
- Open discussion (*) (30 min).

****

**Friday, April 21st, 10:30-12:00 CEST** \
Agenda
- Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10 min)
- Sébastien Lejeune — OSS Community manager (Thales Group). Return of experience on Open Source Software & Hardware governance in Thales. \
  [View slides](/resources/onramp_20230421/20230421_OSPO_OnRamp_Thales_OSPO.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1682064908249)
- Open discussion (*) (40 min).

****

**Friday, March 17th, 10:30-12:00 CET** \
Agenda
- Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10 min)
- Philippe Bareilles — Open Source Program Officer (City of Paris). Paris Open Source — The City's commitment to Open SOurce and roadmap \
  [View slides](/resources/onramp_20230317/20230317_OSPO_OnRamp_Paris_OSPO.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1679045082451)
- Open discussion (*) (20 min).

****

**Friday, February 17th, 10:30-12:00 CET** \
Agenda
- Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (15 min)
- Gabriel Ku Wei Bin — Legal Coordinator (FSFE). FSFE’s Legal Network and its Legal and Licensing Workshop (LLW) \
  [View slides](/resources/onramp_20230217/20230217_OSPO_OnRamp_FSFE_Legal_Network.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1676624644585)
- Open discussion (*) (20 min).

****

**Friday, January 20th, 10:30-12:00 CET** \
Agenda
- Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (15 min)
- Mikael Barbero - Head of Security (Eclipse Foundation). Open Source Software Supply Chain Security — Why does it matter? \
  [View slides](/resources/onramp_20230120/2023_01_OSPO_OnRamp_Open_Source_Software_Supply_Chain_Security_Why_does_it_matter.pdf) - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1674205066957)
- Open discussion (*) (20 min).

****

**Friday, December 16th, 10:30-12:00 CET** \
Agenda
- Michael Plagge (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Boris Baldassari, Florent Zara (Eclipse Foundation) - State of the (OSPO) Alliance: talks, meetings, members, projects progress, and so on. \
  [View slides](/resources/onramp_20221216/221216_OSPO_OnRamp_State_of_the_Alliance.pdf) - [Watch video](https://peertube.xwiki.com/w/3rcCUc4Zs4ZD2Dmii5v8rC)
- Boris Baldassari (Eclipse Foundation) - Live demonstration of the brand new GGI deployment feature.
- Open discussion (*) on 2023 Roadmap and the future of the OSPO Alliance

****

**Friday, October 21st, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Frédéric Pied and François Desmier ([MAIF](https://maif.fr/)). MAIF : a singular open-source strategy. \
  [View slides][def]
- Open discussion (*) (30min).

****

**Friday, September 16th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Florent Zara (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Camille Moulin and Benjamin Jean (InnoCube) - Hermine project: Open source code and data for legal compliance \
  [View slides](/resources/onramp_20220916/20220916_OSPO_OnRamp_Hermine_v03.pdf)
- Open discussion (*) (30min).

****

**Friday, June 17th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Lucian Balea (RTE) - Setting up an OSPO at a power utility \
  [View slides](/resources/onramp_20220617/220617_OSPO_OnRamp_Building_an_OSPO_at_RTE.pdf) - [Watch video](https://peertube.xwiki.com/w/bDWqEvU5twosD2J1jpYj3f)
- Open discussion (*) (30min).

****

**Friday, May 20th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Josep Prat (Aiven) - Building an OSPO: 3rd Party Outbound OSPO (35mn)\
  [View slides](https://jlprat.github.io/3rd-party-outbound-ospo/index.html) - [Watch video](https://peertube.xwiki.com/w/jXnZBWsiZ4spFaD6aawxfw)
- Open discussion (*) (30min).

****

**Friday, March 18th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Boris Baldassari (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Daniel Izquierdo (Bitergia) - Establishing metrics early as an integral part of your OSPO first steps (35mn)\
  [Download slides](/resources/onramp_20220318/20220318_OSPO_OnRamp_Establishing_metrics_OSPO_first_steps.pdf) - [Watch video](https://peertube.xwiki.com/w/dwt4WL7AXvBVnfyRgdfpaw)
- Open discussion (*) (30min).

****

**Friday, February 18th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge, Gaël Blondelle (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Carsten Emde (OSADL) - How to facilitate Open Source software licensing and get it right? (35mn)
- Open discussion (*) (30min).

****

**Friday, January 14th, 10:30-12:00 CET**  \
Agenda:
- Michael Plagge, Gaël Blondelle (Eclipse Foundation) - Update on the OSPO Alliance (10min).
- Henrik Plate (SAP) - [Manage Log4Shell and other open-source vulnerabilities with Eclipse Steady](/resources/onramp_20220114/20220114_OSPO_Alliance_Log4Shell.pdf) (35min).
- Open discussion (*) (30min).

****

**Friday, December 10th, 10:30-12:00 CET** \
Agenda:
- Michael Plagge (Eclipse Foundation) [Intro into the OSPO OnRamp concept](/resources/onramp_20211210/20211210_OSPO_OnRamp.pdf) - Call for active participation (10min).
- Michael Picht (SAP) - [The SAP OSPO](/resources/onramp_20211210/20211210_ext_ospo_onramp_sap_ospo_overview.pdf) (25min).
- Cédric Thomas (OW2) - [The OW2 OSS Good Governance initiative, an OSPO implementation blueprint](/resources/onramp_20211210/20211210_onramp-ggi-introduction-cedricthomas-CC-BY.pdf) (25min).
- Open discussion (*) (30min).


[def]: /resources/onramp_20221021/20221021_OSPO_OnRamp_MAIF.pdf
