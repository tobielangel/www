---
title: "GGI: Conclusion"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----

# Conclusion

As we have said before, open source good governance is not a destination; it's a journey. We need to care about our common assets, about the communities and ecosystem that make it strive, because our own common, and thus individual, success depends on it.

**We**, as software practitioners and open source enthusiasts, are committed to continue improving the Good Governance Initiative handbook and work on its dissemination and reach. We strongly believe that organisations, individuals, and communities need to work hand in hand to build a better and greater set of commons, available and beneficial to all.

**You** are welcome to join the OSPO Alliance, contribute to our work, spread the word, and be the ambassador of a better open source awareness and governance within your own ecosystem. There are a breadth of resources available out there, from blog posts and research articles to conferences and online training courses. We also provide a set of useful material on [our website](https://ospo-alliance.org), and we are happily willing to help as much as we can.

**Let's define and build together the future of the Good Governance Initiative!**

## Contact

The preferred way to get in touch with the OSPO Alliance is to post a message on our public mailing list at <https://accounts.eclipse.org/mailing-list/ospo.zone>. You can also come and discuss with us at the usual open source events, join our monthly OSPO OnRamp webinars, or get in touch with any member -- they will kindly redirect you to the right person.

## Appendix: Customised Activity Scorecard template

The latest version of the Customised Activity Scorecard template is available in the `resources` section of the [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) at OW2.
