---
title: "Translations"
date: 2023-04-10T10:00:00+02:00
show_featured_footer: false
layout: "single"
---

We are always looking for new translators who can help with the translation or review of our Handbook. We use the hosted services from Weblate, thanks to their generous policy of [free hosting of open source projects](https://hosted.weblate.org/hosting/).

* Link to the GGI Handbook project on Weblate: https://hosted.weblate.org/engage/ospo-zone-ggi/

[![État de la traduction](https://hosted.weblate.org/widgets/ospo-zone-ggi/-/svg-badge.svg)](https://hosted.weblate.org/engage/ospo-zone-ggi/)


## Status of the translations

[![État de la traduction](https://hosted.weblate.org/widgets/ospo-zone-ggi/-/multi-auto.svg)](https://hosted.weblate.org/engage/ospo-zone-ggi/)


## Helping with translations

It's really easy to start or update a translation:

> In a web browser, open https://hosted.weblate.org/projects/ospo-zone-ggi/#languages, login or create an account if needed, on the line with the language you will translate to, click on the figure in the "incomplete" column (coordinates: your language/"incomplete") and start translating.

More information about translations, how to contribute and build the translated handbooks, is available in the TRANSLATING.md file in our [GitLab repository](https://gitlab.ow2.org/ggi/ggi/-/blob/main/TRANSLATING.md).
